//
//  MathsStuffTestCases.swift
//  UnitTestCases_ForApiTests
//
//  Created by Sumit Vishwakarma on 24/05/23.
//

import XCTest
@testable import UnitTestCases_ForApi

class MathsStuffTestCases: XCTestCase {
    let math = MathsStuff()
    
    
    //MARK: Addition Test
    func testCheck_AdditionStuff() {
        let result = math.additionStuff(x: 10, y: 20)
        XCTAssertEqual(result, 30)
        XCTAssertTrue(result == 30)
    }
    
    //MARK: Division Test
    func testCheck_DivisionStuff() {
        let result = math.devideStuff(x: 15, y: 3)
        XCTAssertEqual(result, 5)
        XCTAssertTrue(result == 5)
    }
    
    //MARK: Multiply Test
    func testCheck_MultiplyStuff() {
        let result = math.mutiplyStuff(x: 4, y: 4)
        XCTAssertEqual(result, 16)
        XCTAssertTrue(result == 16)
    }
}
