//
//  UserApiTestCase.swift
//  UnitTestCases_ForApiTests
//
//  Created by Sumit Vishwakarma on 24/05/23.
//

import XCTest
@testable import UnitTestCases_ForApi

class UserApiTestCase: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //MARK: Check APi Url is null or not
     func testApiURL() {
        let url = NSURL(string: StringContant.userApiUrl)
        if url != nil {
            XCTAssertTrue(true, "Valid Url")
        } else {
            XCTAssertTrue(false)
        }
    }
    
    
    //MARK: Check User from api are null or not
     func testGetAllUsers_CheckNil() {
        let expectation = self.expectation(description: "need to get users")
        var arrayOfUsers: [User] = []
        
        ApiHandler.getApiCall(serverUrl: StringContant.userApiUrl, for: UsersDataModel.self) { result in
            switch result {
            case .success(let response):
                arrayOfUsers = response.users
                expectation.fulfill()
            case .failure(_):
                expectation.fulfill()
            }
        }
        waitForExpectations(timeout: 15, handler: nil)
        XCTAssertTrue(arrayOfUsers.count > 0)
        XCTAssertEqual(arrayOfUsers.count, 5)
    }
    
    
    //MARK: Check age of User from api
     func testApiUserAge() {
        let expectation = self.expectation(description: "need to Check user age")
        var arrayOfUsers: [User] = []
        
        ApiHandler.getApiCall(serverUrl: StringContant.userApiUrl, for: UsersDataModel.self) { result in
            switch result {
            case .success(let response):
                arrayOfUsers = response.users
                for user in arrayOfUsers {
                    XCTAssertTrue(user.age > 18 ? true : false)
                }
                expectation.fulfill()
            case .failure(_):
                expectation.fulfill()
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
}
