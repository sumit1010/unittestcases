//
//  MathStuff.swift
//  UnitTestCaseDemoApp
//
//  Created by Sumit Vishwakarma on 23/03/23.
//

import Foundation

class MathsStuff {
    
    func additionStuff(x: Int , y: Int) -> Int {
        x + y
    }
    
    func devideStuff(x: Int , y: Int) -> Int {
        x / y
    }
    
    func mutiplyStuff(x: Int , y: Int) -> Int {
        x * y
    }
}
