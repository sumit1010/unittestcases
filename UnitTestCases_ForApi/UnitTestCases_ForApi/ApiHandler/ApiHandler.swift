
import Foundation


//MARK: - Enum for Api Failure
enum NetworkError: Error {
    case InvalidURL
    case noDataAvailable
    case canNotProceedData
}


//MARK: - Api Handler For HTTP Request
public class ApiHandler {
    
    //MARK: Get Api to get Login Data From Server
    static func getApiCall<T: Decodable>(serverUrl url: String,for: T.Type = T.self, completionHandler: @escaping (Result<T, NetworkError>) -> Void) {
        
        let apiUrl = URL (string: url.trimmingCharacters(in: .whitespacesAndNewlines))
        guard let serverUrl = apiUrl else {
            completionHandler(.failure(NetworkError.InvalidURL))
            return
        }
        URLSession.shared.dataTask(with: serverUrl) { (data, response, error) in
            if error == nil {
                guard data != nil else {
                    completionHandler(.failure(NetworkError.noDataAvailable))
                    return
                }
                do {
                    guard let serverData = data else {
                        completionHandler(.failure(NetworkError.noDataAvailable))
                        return
                    }
                    //-- Get Api Respones here
                    let response = try JSONDecoder().decode(T.self, from: serverData)
                    completionHandler(.success(response))
                    
                } catch let error {
                    print (error)
                    completionHandler(.failure(NetworkError.canNotProceedData))
                }
            } else {
                completionHandler(.failure(NetworkError.InvalidURL))
            }
        }.resume()
    }
}

