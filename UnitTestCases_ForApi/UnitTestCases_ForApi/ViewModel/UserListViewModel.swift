
import UIKit


//MARK: Api success/ failure protocol
protocol UserListViewModelDelegate {
    func getAllUsersList(arrayOfUser: [User]?)
}

class UserListViewModel {
    var delegate: UserListViewModelDelegate?
    var arrayOfUser: [User] = []
    
    init() {
        print("LoginViewModel initialized...")
    }
    
    //MARK: Api To get User From Api
    func callApiToGetUserData() {
        
 
            ApiHandler.getApiCall(serverUrl: "https://dummyjson.com/users?limit=5&skip=10&select=firstName,age,lastName", for: UsersDataModel.self) {  result in
                switch result {
                case .success(let response):
                    DispatchQueue.main.async {
                        if response.users.count > 0 {
                            
                            // -- Save Api Data here
                             self.updateApiResponseHandlerDelegateMethod(arrayOfUser: response.users)
                        } else {
                            print("No Data found")
                            self.updateApiResponseHandlerDelegateMethod(arrayOfUser: nil)
                        }
                    }
                case .failure(let error):
                    print("Login api response is: \(error.localizedDescription)")
                    self.updateApiResponseHandlerDelegateMethod(arrayOfUser: nil)
                }
            }
 
    }
    
    func updateApiResponseHandlerDelegateMethod(arrayOfUser: [User]?) {
        guard let arrayOfUser = arrayOfUser else {
            self.delegate?.getAllUsersList(arrayOfUser: nil)
            return }
        self.delegate?.getAllUsersList(arrayOfUser: arrayOfUser)
     }
}


